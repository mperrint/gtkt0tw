//includes
#include "tools.hh"
#include <algorithm>
#include <vector>
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
int GetUID(int ix, int iy, int x0, int y0){
  return (iy-y0)*40+(ix-x0)%40;
}


void CheckPixels(TH3F* h, int s, int c, bool mask[40][45],TFile* fout , const char* repo ){
  TH2F* hMap = new TH2F("hMap",Form("Hit Map GTK%d Chip%d; X [pixel]; y [pixel]",s,c),40,0,40,45,0,45);
  TH2F* hMapNoise = new TH2F("hMapNoise",Form("Noise Map GTK%d Chip%d; X [pixel]; y [pixel]",s,c),40,0,40,45,0,45);

  vector<double> vc;
  vc.reserve(1800);

  for(int iX(0);iX<40;iX++ ){
    for(int iY(0);iY<45;iY++ ){
      double nC = h->Integral(0,-1,0,-1, iX*45+iY+1, iX*45+iY+1 );
      hMap->Fill(iX,iY,nC);
      vc.push_back(nC);
    }
  }
  std::sort(vc.begin(), vc.end());
  double th1 = vc[0.5*vc.size()];
  double th2 = vc[0.1*vc.size()];
  for(int iX(0);iX<40;iX++ ){
    for(int iY(0);iY<45;iY++ ){
      if(hMap->GetBinContent(iX+1,iY+1) < 10*th1 && hMap->GetBinContent(iX+1,iY+1) >0.1*th2) continue;
      mask[iX][iY]=1;
      hMapNoise->Fill(iX,iY);
      if(hMap->GetBinContent(iX+1,iY+1) > 10*th1)   cout<<"    pixel "<<iX<<" - "<<iY<<" is noisy"<<endl;
      if(hMap->GetBinContent(iX+1,iY+1) < 0.1*th2)  cout<<"    pixel "<<iX<<" - "<<iY<<" is disconnected"<<endl;
    }
  }
  if(fout != NULL ){
    if(fout->GetDirectory(repo) == NULL) {
      fout->mkdir(repo);
    }
    fout->cd(repo);
    hMap->Write();
    hMapNoise->Write();
  } 
  cout<<"--> done!"<<endl;
  return;
}

/*
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
double GetBestToT(string filePathIn, string refDet, int s, int c, TFile* fout , const char* repo    ){

  TH2F* hTot = new TH2F("htot","Peak ToT [ns]; X [pixel]; Y [pixel]",40,(c%5)*40,(c%5+1)*40,45,c/5 * 45,(c/5 + 1)*45);
  TH2F* hBestTot;// = new TH2F("hbesttot","; ToT [ns]; Pixel Id",410,0,40,1800,0,1800);

  int xM,yM,zM, iH(0);
  double tot;

  int x0 = (c%5) * 40;
  int y0 = (c/5) * 45;

  TFile f(filePathIn.c_str());
  for(int iX(x0);iX<(x0+40);iX++ ){
    for(int iY(y0);iY<y0+45;iY++ ){
      //cout<<"--> Pixel "<<iH+1<<" / 1800 \r";
      //std::cout.flush();

      TH2F* h= (TH2F*) f.Get(Form("GigaTracker_Tzero%s/GTK%d/Chip%d/dt_tot_%d_%d-%d",refDet.c_str(),s,c,s,iX,iY));
       if(iH==0) {
       	double binsX[411];
       	binsX[0] = 0;
       	for (int i(1);i<411;i++) binsX[i] = h->GetXaxis()->GetBinUpEdge(i);
       	hBestTot = new TH2F("hbesttot","; ToT [ns]; Pixel Id",410,binsX,1800,0,1800);
	hBestTot->SetDirectory(0);

       }
      h->GetBinXYZ(h->GetMaximumBin(),xM,yM,zM);
      tot = h->GetXaxis()->GetBinCenter(xM);
      hTot->SetBinContent(iH%40 + 1,iH/40 + 1,tot);

      TH1D * lproj = h->ProjectionX();
      for( int j(0); j<410;j++){
	if(lproj->GetBinContent(j+1)>500) lproj->SetBinContent(j+1,500);
	hBestTot->SetBinContent(j+1,iH+1, lproj->GetBinContent(j+1) );
      }
      h->Delete();
      iH++;
    }
  }
  f.Close();
  //cout<<endl;
  cout<<"--> done!"<<endl;
  
  TH1D * proj = hBestTot->ProjectionX();
  double peak = proj->GetXaxis()->GetBinCenter(proj->GetMaximumBin());

  if(fout != NULL ){
    if(fout->GetDirectory(repo) == NULL) {
      fout->mkdir(repo);
    }
    fout->cd(repo);
    hTot->Write();
    hBestTot->Write();
    proj->SetName("hBestToTp");
    proj->SetTitle("Best ToT");
    proj->Write();
  }

  hTot->Delete();
  proj->Delete();
  hBestTot->Delete();


  return peak;
}
*/

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
int GetTZero(TH2D* h, double& t0, double tot, TFile* fout, const char* repo ){

  TH1D* proj;
  if(tot>0){
    int sbin = h->GetXaxis()->FindBin(tot);
    proj = h->ProjectionY(Form("t0_%s",h->GetName()),sbin,sbin+1);
  }
  else {
    int sbin0 = h->GetXaxis()->FindBin(5);
    int sbin1 = h->GetXaxis()->FindBin(19);
    proj = h->ProjectionY(Form("t0_%s",h->GetName()),sbin0,sbin1);
  }
  //SmoothT0Histo(proj);

  double center = proj->GetXaxis()->GetBinCenter(proj->GetMaximumBin());
  TF1 f1("f1","gaus",-20,20);

  f1.SetParameter(1,center);
  f1.SetParameter(2,0.240);
  proj->Fit(&f1,"Q","",center-0.4,center+0.4);

  if(fout != NULL ){
    if(fout->GetDirectory(repo) == NULL) {
      fout->mkdir(repo);
    }
    fout->cd(repo);
    proj->SetTitle(Form("%s center %2.3f",proj->GetTitle(),center));
    proj->Write();
  }
  proj->Delete();
  t0 =  f1.GetParameter(1);

  return 1;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void ApplyTW(TH2D* h, float* tw){
  TH2D* hh= (TH2D*) h->Clone();
  h->Reset();
  for (int iX(1); iX<hh->GetXaxis()->GetNbins()+1; iX++){
    for (int iY(1); iY<hh->GetYaxis()->GetNbins()+1; iY++){
      float x = hh->GetXaxis()->GetBinCenter(iX);
      float y = hh->GetYaxis()->GetBinCenter(iY) - tw[iX-1];
      double s = hh->GetBinContent(iX,iY);
      h->Fill(x,y,s);
    }
  }
  hh->Delete();
  return;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
TH2D GrpHist(TH2D** vh, int s){
  TH2D* hh = (TH2D*) vh[0]->Clone();
  hh->Reset();
  for(int i(0); i<s; i++){
    hh->Add(vh[i]);
  }  
  return *hh;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
bool isGoodSlice(TH1D* h , int min ){
  double inte = h->Integral();
  if( inte < min ) return 0;
  double peakSig = h->GetMaximum()*h->GetNbinsX()/inte;
  return 1;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
double FitSlice(TH1D* h, TF1* f1,int nSlice){
  double center(0);
  if (nSlice == 0 ) center = h->GetXaxis()->GetBinCenter(h->GetMaximumBin());
  else   center = f1->GetParameter(1);
  h->GetXaxis()->SetRangeUser(center-0.5,center+0.5);
  h->Fit(f1,"Q","",center-0.4,center+0.4);
  //  h->Fit(f1,"Q","",center-0.8,center+0.8); //remove mpt
  //f1->SetParLimits(1,center-2,center+2); //added
  //h->Fit(f1,"Q","",center-2,center+2);  //added

  center = f1->GetParameter(1);
  f1->SetParLimits(1,center-0.3,center+0.3); //was 0.4

  double width = f1->GetParameter(2);
  f1->SetParLimits(2,0,width+0.5);
  return f1->GetParameter(1);
}



//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
bool ProcessSlice(TH2D* h, int& start,int& stop, float* tw, TF1* f1, int& nSlice, int dir , int min ){
  char sliceName[100];
  double sliceLow = h->GetXaxis()->GetBinLowEdge(start);
  double sliceUp = h->GetXaxis()->GetBinLowEdge(stop);
  sprintf(sliceName,"sl_%d_%.1f-to-%.1f",nSlice,sliceLow, sliceUp);
  TH1D* slice =  h->ProjectionY(sliceName,start,stop);
  slice->SetTitle(sliceName);
  if(!isGoodSlice(slice, min)) return 0;
  tw[stop-1] = FitSlice(slice,f1,nSlice);
  for(int i(start);i<=stop-1;i++) {
    tw[i]=tw[stop-1];
  }

  if(dir>0) start++;
  if(dir<0) stop--;
  nSlice++;
  slice->Delete();
  return 1;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void GetTW(TH2D* h, float* tw,  int min , TFile* fout,const char* repo  , bool svH ){

  int nBins = h->GetXaxis()->GetNbins();
  int nSlice(0);
  int xM,yM,zM;
  h->GetBinXYZ(h->GetMaximumBin(),xM,yM,zM);
  int start = xM;
  int stop = start+1;
  TF1  f1 ("f1","gaus",-10,10);
  TF1  f2 ("f2","gaus",-10,10);
  ProcessSlice(h,start, stop, tw, &f1,nSlice, 1, min);
  for(int p(0);p<3;p++)  f2.SetParameter(p, f1.GetParameter(p));

  for(stop++; stop<= nBins; stop++){
    if( ! ProcessSlice(h,start,stop,tw,&f1,nSlice, 1, min) ) continue;
    if( fabs(stop-start) > 30 ) { 
      break;
    }
  }

  start = xM-1;
  stop = start+1;
  for(; start>0; start--){
    if( ! ProcessSlice(h,start,stop,tw,&f2,nSlice, -1, min) ) continue;
    if( fabs(stop-start) > 30 ) { 
      break;
    }
  }


  int i(0);
  while(tw[i]==0 && i<nBins-1) i++;
  while(i>0 && tw[i-1]==0){
    tw[i-1] = tw[i];
    i--;
  }

  i = nBins-1;
  while(tw[i]==0 && i>0) i--;
  while(i<nBins && tw[i+1]==0){
    tw[i+1] = tw[i];
    i++;
  }

  if(fout != NULL ){
    float twc[410];
    for(i=0;i<nBins;i++){
      twc[i] = h->GetXaxis()->GetBinCenter(i+1);
    }
    TGraph gr(nBins,twc,tw);
    gr.SetName(Form("%s_gr",h->GetName()));
    gr.SetTitle(h->GetTitle());
    if(fout->GetDirectory(repo) == NULL) fout->mkdir(repo);
    fout->cd(repo);
    if(svH) h->Write();
    gr.Write();
    gr.Delete();
  }

}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void GetTResoMean(TH2D* h, double& reso, double& mean, TFile* fout, const char* repo){

  TH1D * proj = h->ProjectionY();
  double center = proj->GetXaxis()->GetBinCenter(proj->GetMaximumBin());
  TF1 f1("f1","gaus",-20,20);
  f1.SetParameter(1,center);
  f1.SetParameter(2,0.230);

  proj->Fit(&f1,"Q","",center-0.3,center+0.3);

   if(fout != NULL ){
     if(fout->GetDirectory(repo) == NULL) fout->mkdir(repo);
     fout->cd(repo);
     proj->Write();
  }

  mean =  f1.GetParameter(1);
  reso =  f1.GetParameter(2);
  return;

}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void SmoothTWHisto(TH2D* h){
  int nX = h->GetNbinsX();
  int nY = h->GetNbinsY();
  for(int iX(1); iX<=nX;iX++){
    for(int iY(1); iY<=nY;iY++){
      if(h->GetBinContent(iX,iY)<10) continue;
      //compute average
      double average = 0;
      double npoint = 0;
     for(int dx(-2);dx<=2;dx++){
	for(int dy(-2);dy<=2;dy++){
	  if(dx==0 && dy==0 ) continue;
	  if(iX+dx > nX || iX+dx<=0) continue;
	  if(iY+dy > nY || iY+dy<=0 ) continue;
	  double area = h->GetXaxis()->GetBinWidth(iX+dx)* h->GetYaxis()->GetBinWidth(iY+dy);
	  average += (h->GetBinContent(iX+dx,iY+dy))/(area) ;
	  npoint += 1;
	}
     }
     if(npoint == 0 ) continue;
     average /= npoint;
     double area = h->GetXaxis()->GetBinWidth(iX)* h->GetYaxis()->GetBinWidth(iY);
     if(h->GetBinContent(iX,iY)/area > 2.5*average){
       printf("TW: Bin %d-%d histo %s reset from %f to %f\n",iX,iY,h->GetName(),h->GetBinContent(iX,iY),average*area);
       h->SetBinContent(iX,iY,average*area);

     }
    }
  }
  return;
}

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void SmoothT0Histo(TH1D* h){
  int nX = h->GetNbinsX();
  for(int iX(1); iX<=nX;iX++){
    if(h->GetBinContent(iX)<50) continue;
    //compute average
    double average = 0;
    double npoint = 0;
    double err = 0;
    for(int dx(-7);dx<=7;dx++){
      if(dx==0) continue;
      if(iX+dx > nX || iX+dx<=0) continue;
      if(h->GetBinError(iX+dx) == 0 ) continue;
      average += h->GetBinContent(iX+dx) / h->GetBinError(iX+dx) ;
      err += 1/h->GetBinError(iX+dx);
      npoint += 1;
    }
    if(npoint == 0 ) continue;
    average /= err;
    if(h->GetBinContent(iX) > 3*average){
      printf("T0: Bin %d (%f) histo %s reset from %f to %f\n",iX,h->GetBinCenter(iX),h->GetName(),h->GetBinContent(iX),average);
      h->SetBinContent(iX,average);
    }
  }
  return;
}

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void SmoothT0(float vt0[40][45], bool masks[40][45], TFile* fout, const char* repo1, const char* repo2 ){
  int nX = 40;
  int nY = 45;
  for(int iX(0); iX<nX;iX++){
    for(int iY(0); iY<nY;iY++){

      //compute average
      double average = 0;
      double npoint = 0;
      for(int dx(-3);dx<=3;dx++){
	for(int dy(-1);dy<=1;dy++){
	  if(dx==0 && dy==0 ) continue;
	  if(iX+dx >= nX || iX+dx<0    ) continue;
	  if(iY+dy >= nY || iY+dy<0    ) continue;
	  if(masks[iX+dx][iY+dy]       ) continue;
	  average += vt0[iX+dx][iY+dy];
	  npoint += 1;
	}
     }
     if(npoint == 0 ) continue;
     average /= npoint;

     if(masks[iX][iY] == 1){
       printf("    T0 of Masked Pixel %d-%d set to %f\n",iX,iY,average);
       vt0[iX][iY] = average;
     }
     if(fabs(vt0[iX][iY] - average) > 2 ){
       printf("    T0 of Pixel %d-%d reset from %f to %f, please check\n",iX,iY,vt0[iX][iY],average);
       vt0[iX][iY] = average;
       TH1D* h = (TH1D*) fout->Get(Form("%s/t0_h_dt_tot_x%d_y%d",repo2,iX,iY));
       h->SetTitle(Form("%s to %2.3f ",h->GetTitle(),average));
       
       if(fout!=NULL){
	 if(fout->GetDirectory(repo1) == NULL) {
	   fout->mkdir(repo1);
	 }
	 fout->cd(repo1);
	 if(h==NULL) {
	   cout<<"Histo "<< Form("%s/t0_h_dt_tot_x%d_y%d",repo2,iX,iY)<<" is missing"<<endl;
	   continue;
	 }

	 h->Write();
       }
     }
    }
  }
  return;
}

void CheckTW(float* tw, double* oldbins, double* newbins, double& nbins){
  int slope[409];
  for(int i(0);i<409;i++){
    if(tw[i+1] > tw[i]) slope[i] = 1;
    if(tw[i+1] < tw[i]) slope[i] = -1;
    if(tw[i+1] == tw[i]) slope[i] = 0;
  }
  

  int iB(0);
  newbins[iB] = oldbins[0];
  for(int i(0);i<409;i++){
    int nc(0);
    for(int j(0); i<10;j++){
      if(slope[i]!=slope[i+j]) nc++;
    }
    if(nc<=2) {
      iB++;
      newbins[iB] = oldbins[i];
    }
    else {
      while (slope[i]==slope[i+1] && i<409) {
	i++;
	iB++;
	newbins[iB] = oldbins[i];
      }
    }
  }
  if(newbins[iB] != oldbins[408]) newbins[iB+1] = oldbins[409];
  else newbins[iB] = oldbins[409];
  nbins = iB+1;
  
}
