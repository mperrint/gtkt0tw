#include "TGraph.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TString.h"
#include <stdio.h>
#include <stdio.h>
#include "TStyle.h"
//#include "TIter.h"
#include "TKey.h"
#include "TClass.h"
#include "TROOT.h"
#include "string.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
using namespace std;
int main(int argc, char **argv)
{


  int option(0);
  const char* fileIn;
  const char* fileOut;
  while ((option = getopt(argc, argv,"i:o:h")) != -1) {
    switch (option) {
    case 'i' : {
      fileIn = optarg;
      struct stat st;
      if(stat(optarg, &st)!=0) {
	cerr<<"File"<<optarg<<" does not exist"<<endl;
	exit(1);
      }
      break;
    }
    case 'o' : {
      fileOut = optarg;
      break;
    }
    case 'h': {
      printf("Usage: ./bin/summary -i listMathieu.txt -o output/Run8252/sumMathieu.pdf \n");
      printf("Options:\n -i : list of input root file, ordered: GTK0 chip0->9 GTK1 chip0->9 GTK2 chip0->9\n -o output root file (with extension)\n");
      exit(0);
    }
    }
  }

  gStyle->SetOptStat(0);  
  TString files[30];
  TGraph* tws[30];
  TMultiGraph* mgtw = new TMultiGraph();
  TLegend* leg = new TLegend(0.75,0.5,0.9,0.9);
  int colors[10]={1,2,3,4,5,6,7,8,9,10};

  char* line = NULL;
  size_t len = 0;
  ssize_t nChars = 0;
  FILE*  list = fopen(fileIn,"r");
  int f(0);
  while( ( nChars = getline(&line,&len,list)) !=-1 ){
    line[nChars - 1] = '\0';
    files[f]=line;
    f++;
  }

  TCanvas cv;
  cv.SetGrid(1,1);
  TH2F* hxy = new TH2F("hxy","",2,0,40,2,-8,8);
  TH2F* ht0 = new TH2F("ht0","Pixels T0",200,0,200,90,0,90);


  cv.Print(Form("%s[",fileOut));
  for(int i(0);i<f;i++){

    struct stat st;
    if(stat(files[i].Data(), &st)!=0) {
      cerr<<"File"<<files[i].Data()<<" does not exist"<<endl;
      exit(1);
    }
    
    TFile* f = new TFile(Form("%s",files[i].Data()),"READ");
    printf("Processing file %s\n",files[i].Data());
    TH2F* h = (TH2F*) f->Get("tw-final");
    std::string str = std::string(h->GetTitle());
    tws[i] = (TGraph*) f->Get("tw-final_gr");
    hxy->SetTitle(h->GetTitle());
    hxy->Draw();
    cv.SetLogz();
    h->Draw("SAMECOLZ");
    tws[i]->Draw("SAME");
    tws[i]->SetLineColor(colors[i%10]);
    mgtw->Add(tws[i],"l");
    int nbChar = files[i].Length();
    TString legTxt = files[i](nbChar-14,9);
    leg->AddEntry(tws[i], legTxt.Data(),"l");
    cv.Print(fileOut);

    TH2D* h2 = (TH2D*) f->Get("t0-final");
    h2->Draw("COLZ");
    for (int iX(0); iX < h2->GetXaxis()->GetNbins(); iX++) {
      double x = h2->GetXaxis()->GetBinCenter(iX+1);
      for (int iY(0); iY < h2->GetYaxis()->GetNbins(); iY++) {
	double y = h2->GetYaxis()->GetBinCenter(iY+1);
	ht0->Fill(x,y,h2->GetBinContent(iX+1,iY+1));
      }
    }
          // T0 to check
    if(f->GetDirectory("t0-1/check") != NULL) {
      f->cd("t0-1/check");
      
      cout<<"CHECK PIXEL T0 "<<files[i]<<endl;
      TIter next(f->GetDirectory("t0-0/check")->GetListOfKeys());
      TKey *key;
      while ((key = (TKey*)next())) {
	TClass *cl = gROOT->GetClass(key->GetClassName());
	if (!cl->InheritsFrom("TH1")) continue;
	TH1 *h = (TH1*)key->ReadObj();
	cout<<"Draw "<<h->GetName()<<endl;
	h->Draw();
	cv.Print(fileOut);
      }
      f->cd();
    }
    
    if(i%10==9) {
      
      // All the TW surimposed
      TH2D h("h","",10,0,30,10,-8,8);
      h.Draw();
      mgtw->Draw("lp");
      leg->Draw();
      cv.Print(fileOut);
      mgtw->Delete();
      mgtw = new TMultiGraph();
      leg->Delete();
      leg = new TLegend(0.75,0.5,0.9,0.9);

      // T0 Maps
      ht0->SetTitle(Form("Pixel T0 %s",str.substr(0, 5).c_str()));
      ht0->Draw("COLZ");
      cv.SetLogz(0);
      cv.Print(fileOut);
      ht0->Reset();
      
    }
    h2->Delete();
    f->Close();
    f->Delete();


  }
  cv.Print(Form("%s]",fileOut));

}
