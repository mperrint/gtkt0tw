#include <stdio.h>
#include <iostream>
#include <getopt.h>


#include "TFile.h"
#include "TH2D.h"
#include "TH3F.h"
#include "TCanvas.h"

using namespace std;
int main(int argc, char **argv)
{
  
  int option(0);
  const char* fileIn;
  const char* fileOut;
  while ((option = getopt(argc, argv,"i:o:")) != -1) {
    switch (option) {
    case 'i' : {
      fileIn = optarg;
      break;
    }
    case 'o' : {
      fileOut = optarg;
      break;
    }
    }
  }

  TFile* fin = new TFile(fileIn);
  TCanvas*  cv = new TCanvas("cv","",600,400);
  cv->Print(Form("%s[",fileOut));
  for (int iS(0); iS < 3; iS++) {
    for (int iC(0); iC < 10; iC++) {
      TH3F* h3 = (TH3F*) fin->Get(Form("GigaTrackerPixelT0ChipTW/h_ktag_gtk%dc%d_dtToTPix",iS+1,iC));
      cout<<h3->GetEntries()<<endl;
      TH2D* hh = (TH2D*) h3->Project3D("xy");
      cout<<hh<<endl;
      cv->cd();
      hh->DrawCopy("COLZ");
      cv->Print(Form("%s",fileOut));
    }

  }
  cv->Print(Form("%s]",fileOut));
 
  return 0;
}
