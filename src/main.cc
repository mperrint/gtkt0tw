//includes
#include "TFile.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TH3D.h"
#include "TF1.h"
#include "TAxis.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TCanvas.h"
#include <iostream>
#include <TError.h> 
#include "TROOT.h"
#include "TRint.h"
#include "RooMinuit.h"
#include "tools.hh"
#include <sys/types.h>
#include <sys/stat.h>
using namespace std;



//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// MAIN
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
int main(int argc, char **argv){

  using namespace std;

  //###### Options Parsing ######
  int option = 0;
  string filePathIn, filePathOut, refDet("ktag"), outDir("");
  
  int  gtk(0), chip(0);

  while ((option = getopt(argc, argv,"i:o:s:c:r:d:h")) != -1) {
    switch (option) {

    case 'i': { //input
      filePathIn = string(optarg);
      struct stat st;
      if(stat(optarg, &st)!=0) {
	cerr<<"File"<<optarg<<" does not exist"<<endl;
	exit(1);
      }
      break;
    }

    case 'o': { //ouput
      filePathOut = string(optarg);
      break;
    }

    case 'd': { //ouput
      outDir = string(optarg);
      struct stat st;
      if( stat(optarg, &st) != 0 || !(S_ISDIR(st.st_mode)) ){
	cerr<<"Dir"<<optarg<<" does not exist"<<endl;
	exit(1);	
      }
      break;
    }

    case 's': {
      gtk = atoi(optarg);
      if( gtk<0 || gtk>2 ){
	cerr<<"Station Id "<<gtk<<" not valid"<<endl;
	exit(1);	
      }
      break;
    }

    case 'c': {
      chip = atoi(optarg);
      if( chip<0 || chip>9 ){
	cerr<<"Chip Id "<<chip<<" not valid"<<endl;
	exit(1);	
      }
      break;
    }
    case 'r': {
      refDet = string(optarg);
      break;
    }
    case 'h': {
      printf("Usage: ./bin/main -i /tmp/mperrint/GigaTracker_Tzero_Tzero_008252-1324.root  -o /tmp/mperrint/aaa.root  -s 0  -c 0 -r ktag -d /tmp/mperrint/ \n");
      printf("Options:\n -i : input root file\n -o output root file\n -s gtk id [0-3]\n -c chip id [0-9]\n -r reference detector [ktag]\n -d output directory where to store corrections\n");
      exit(0);
    }

    }
  }
  printf("\n******** T0 and TW GTK%d CHIP%d *******\n",gtk+1,chip);
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(0);
  gErrorIgnoreLevel = kFatal;
  gStyle->SetPaintTextFormat("3.1f");

  //  FILE* fReso;
  FILE* fCorr;
  FILE* fBinning;
  char buff[400];
  // sprintf(buff,"%s/GigaTracker-time-reso_gtk%d-chip%d.dat",outDir.c_str(),gtk,chip);
  // fReso = fopen(buff,"w");
  // if (fReso == NULL) {
  //   printf("Error cannot open %s\n",buff);
  // }

  int nX = 40;
  int nY = 45;
  int x0 = (chip%5) * 40;
  int y0 = (chip/5) * 45;

  int nBins = 410;
  float* tw = (float*) calloc (nBins,sizeof(float));
  float* t0 = (float*) calloc (nBins,sizeof(float));
  for (int i(0); i<nBins; i++){
    tw[i]=0;
    t0[i]=0;
  }
  
 
  TFile* fout = new TFile(filePathOut.c_str(),"RECREATE");

  float vt0[40][45];
  bool masks[40][45];
  for(int i(0); i<40;i++){
    for(int j(0); j<45;j++){
      vt0[i][j]=0;
      masks[i][j] = 0;		    
    }
  }
  float vtw[410];

  TFile* fin = new TFile(filePathIn.c_str());
  TH3F* h3 =  ( !(strcmp(refDet.c_str(),"")) )?
    (TH3F*) fin->Get(Form("GigaTrackerPixelT0ChipTW/h_gtk%dc%d_dtToTPix",gtk+1,chip)) :
    (TH3F*) fin->Get(Form("GigaTrackerPixelT0ChipTW/h_%s_gtk%dc%d_dtToTPix",refDet.c_str(),gtk+1,chip));
  if(h3==NULL){
    cout<<"Cannot load histo, please check you gave the right reference time"<<endl;
    exit(1);
  }
  nBins = h3->GetXaxis()->GetNbins();
  int iH(0);

  cout<<"Identifying Noisy and Masked Pixels"<<endl;
  CheckPixels(h3,gtk,chip,masks, fout, "hm");

  //cout<<"Looking for best tot"<<endl;
  double  totT0 = 16;
  //for stability ToT is fixed at 16 ns
  //double  totT0 = GetBestToT(filePathIn,refDet,gtk,chip, fout, "tot");


  TH2D* htw1;
  int nStep(2);
  for(int step(0); step<nStep;step++){
    iH = 0;
    cout<<"Extracting t0 step "<<step<<endl;
    double mean(0), reso(0);


    TH2D ht0(Form("ht0-%d",step),Form("T0 evaluated at ToT = %f ns;X [pixel];Y [pixel]",totT0),40,x0,x0+40,45,y0,y0+45);
    TH2D ht0s(Form("ht0s-%d",step),Form("T0 evaluated at ToT = %f ns - Smoothed;X [pixel];Y [pixel]",totT0),40,x0,x0+40,45,y0,y0+45);
    cout<<"\r  Evaluate t0\r"<<endl;
    for(int iX(0);iX<nX;iX++ ){
      for(int iY(0);iY<nY;iY++ ){
	if(masks[iX][iY] == 1) {
	  cout<<"    Skip Pixel "<<iX<<" - "<<iY<<endl;
	  if(iH!=0) iH++;
	  continue;
	}
	cout<<"   > Pixel "<<iH+1<<" / "<< nX*nY <<" \r"; std::cout.flush();

	h3->GetZaxis()->SetRange(iX*45+iY+1, iX*45+iY+1);
	TH2D* hh = (TH2D*) h3->Project3D("yx");
	hh->SetName(Form("h_dt_tot_x%d_y%d",iX,iY));
	TH2D* h = (TH2D*) hh->Clone(); 

	if(step > 0 )  {
	  ApplyTW(h,tw);
	  totT0 = 0;
	  ht0.SetTitle("T0 evaluated with all ToT");
	}

	if(step==0) GetTZero(h,mean,totT0, fout,Form("t0-%d/pix",step));//f->cd(); 
	else        GetTZero(h,mean,-1, fout,Form("t0-%d/pix",step));//f->cd(); 

	vt0[iX][iY] = mean;
	for(int i(0);i<nBins;i++) t0[i]=mean;
	ApplyTW(hh,t0);
	if (iH==0) {
	  htw1=(TH2D*)hh->Clone();
	  htw1->SetTitle(Form("GTK %d Chip %d| All Pixels Pixel T0 + Global TW",gtk,chip));
	  htw1->SetName(Form("dt_tot_all_%d",step));
	  htw1->SetDirectory(0);
	}
	else htw1->Add(hh);
	ht0.Fill(iX+x0,iY+y0,vt0[iX][iY]);
	iH++;
	hh->Delete();
	h->Delete();
      }
    }
    cout<<"\n  Smooth t0"<<endl;
    SmoothT0(vt0,masks,fout,Form("t0-%d/check",step),Form("t0-%d/pix",step));
    //SmoothT0(vt0,masks);
    for(int iX(0);iX<nX;iX++ ){
      for(int iY(0);iY<nY;iY++ ){
	ht0s.Fill(iX+x0,iY+y0,vt0[iX][iY]);
      }
    }

    cout<<"--> done!"<<endl;
    fout->cd(Form("t0-%d",step));
    fout->cd(Form("t0-%d",step));
    ht0.Write();
    ht0s.Write();

    cout<<"Extracting time walk"<<endl;
    //SmoothTWHisto(htw1);
    GetTW(htw1, tw,TMath::Max(int(htw1->GetEntries()/1e4),5000),fout,Form("tw-%d",step),1);

    //ApplyTW(htw1,tw);
    //cout<<"--> done!"<<endl;

    //cout<<"Extracting Time Resolution"<<endl;
    //GetTResoMean(htw1,reso,mean,fout,"reso");//f->cd();
    //fprintf(fReso,"Indicative Time Reso (Pixel T0 + Global TW): %f\n",reso);
    //cout<<"--> "<<  reso*1000 <<" ps"<<endl;
    //cout<<"--> done!"<<endl;

    if(step<nStep-1 ) htw1->Delete();
  }

  // t0 and tw standardisation
  // tw at ToT=16ns is 0
  // average pixel t0 is 0

  cout<<"Normalised Correction"<<endl;
  int bin0 = htw1->GetXaxis()->FindBin(16.)-1;
  double tw0 = tw[bin0];
  cout<<" TW at 16 set to 0: offset by"<<tw0<<endl;
  float twc[410];
  float tw0s[410];
  for(int i(1); i<nBins;i++) {
    tw[i]=tw[i]-tw0;
    tw0s[i]=tw0;
    twc[i] = htw1->GetXaxis()->GetBinCenter(i+1);
  }
  ApplyTW(htw1,tw0s);

  TGraph gr(nBins,twc,tw);
  fout->cd("/");
  gr.Write("tw-final_gr");
  htw1->Write("tw-final");


  TH2D ht0final("t0-final","T0 Normalised;X [pixel];Y [pixel]",40,x0,x0+40,45,y0,y0+45);
  double avT0(0);
  for(int i(0); i<40;i++){
    for(int j(0); j<45;j++){
      avT0 += vt0[i][j];
    }
  }
  avT0 /= (40*45);
  for(int i(0); i<40;i++){
    for(int j(0); j<45;j++){
      vt0[i][j] -= avT0;
      ht0final.Fill(i+x0,j+y0,vt0[i][j]);
    }
  }
  fout->cd("/");
  ht0final.Write();

  //Save t0 and tw 
  sprintf(buff,"%s/GigaTracker-time-walk_binning.dat",outDir.c_str());
  fBinning = fopen(buff,"w");
  if (fBinning == NULL) {
    printf("Error cannot open %s\n",buff);
  }

  sprintf(buff,"%s/GigaTracker-time-walk_gtk%d-chip%d.dat",outDir.c_str(),gtk,chip);
  fCorr = fopen(buff,"w");
  if (fCorr == NULL) {
    printf("Error cannot open %s\n",buff);
  }  

  fprintf(fBinning,"%5.5f\n", htw1->GetXaxis()->GetBinLowEdge(1));
  fprintf(fCorr,"%5.5f\n", tw[0]);
  for(int i(1); i<nBins;i++) {
    //if( fabs(tw[i-1]-tw[i])<0.005 ) continue;
    fprintf(fBinning,"%5.5f\n", htw1->GetXaxis()->GetBinUpEdge(i));
    fprintf(fCorr,"%5.5f\n", tw[i]);
  }
  fprintf(fBinning,"%5.5f\n", htw1->GetXaxis()->GetBinUpEdge(nBins));
  fclose(fCorr);
  fclose(fBinning);

  sprintf(buff,"%s/GigaTracker-t-zero_gtk%d-chip%d.dat",outDir.c_str(),gtk,chip);
  fCorr = fopen(buff,"w");
  if (fCorr == NULL) {
    printf("Error cannot open %s\n",buff);
  }  

  for(int iX(x0);iX<(x0+nX);iX++ ){
    for(int iY(y0);iY<y0+nY;iY++ ){
      int uid = iX + iY*200; //uid goes from 0->17999
      fprintf(fCorr,"%d %5.5f\n",uid, vt0[iX-x0][iY-y0]);
    }
  }
  fclose(fCorr);

  return 1;

}


