#include "TFile.h"
#include "TH2D.h"
#include "TH3F.h"
#include "TF1.h"
#include "TAxis.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TCanvas.h"
#include <iostream>
#include <TError.h> 
#include "TROOT.h"
#include "TRint.h"
#include <string.h>
using namespace std;

int GetUID(int ix, int iy, int x0, int y0);
void CheckPixels(TH3F* h, int s, int c, bool mask[40][45],TFile* fout = NULL , const char* repo ="");
//double GetBestToT(string, string, int s, int c, TFile* fout= NULL , const char* repo = "" );
int GetTZero(TH2D* h, double& t0, double tot, TFile* fout= NULL, const char* repo = "" );
void ApplyTW(TH2D* h, float* tw);
TH2D GrpHist(TH2D** vh, int s);
bool isGoodSlice(TH1D* h , int min = 1000);
double FitSlice(TH1D* h, TF1* f1,int nSlice);
bool ProcessSlice(TH2D* h, int& start,int& stop, float* tw, TF1* f1, int& nSlice, int dir , int min = 1000);
void GetTW(TH2D* h, float* tw,  int min= 1000 , TFile* fout= NULL, const char* repo = "" , bool svH = 0);
void GetTResoMean(TH2D* h, double& reso, double& mean, TFile* fout= NULL, const char* repo = "");
void SmoothTWHisto(TH2D*);
void SmoothT0(float vt0[40][45], bool masks[40][45], TFile* fout= NULL, const char* repo1 = "", const char* repo2 = "");
void SmoothT0Histo(TH1D* h);
void CheckTW(float* tw, double* oldbins, double* newbins, double& nbins);
